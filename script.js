class Hamburger {
    constructor(size, stuffing) {
        try {
            if (!size) {
                throw new HamburgerException("Choose a large or small size hamburger!")
            }
            if (!stuffing) {
                throw new HamburgerException("Choose a stuffing cheese salad or pepper for a hamburger!")
            }
            this.size = size;
            this.stuffing = stuffing;//начинка
            this.topping = [];
        } catch (e) {
            console.log(e);
        }
    }
    calculatePrice () {
        let number = this.size.price + this.stuffing.price;
        for (let topping of this.topping) {
            number += topping.price
        }
        return number;
    };
    calculateCalories () {
        let number = this.size.calories + this.stuffing.calories;
        for (let topping of this.topping) {
            number += topping.calories
        }
        return number;
    };
    addTopping (topping) {
        let newTopping = topping;
        if (this.topping.length !== null) {
            for (let i = 0; i < this.topping.length; i++) {
                if (this.topping[i].name == newTopping.name) {
                    throw new HamburgerException(`Already have one ${newTopping.name}`)
                }
            }
        }
        this.topping.push(newTopping);
        return this.topping;

    };
    removeTopping (topping) {
        if (!topping || !this.topping.includes(topping)) {
            throw new HamburgerException(`no such topping or topping duplicate ${topping.name}`)
        } // !topping
        return this.topping.splice(this.topping.indexOf(topping), 1);
    };
    size () {
        return this.size;
    };
    topping () {
        return this.topping;
    };

}
Hamburger.SIZE_SMALL = {
    price: 50,
    calories: 20
};
Hamburger.SIZE_LARGE = {
    price: 100,
    calories: 40
};

Hamburger.STUFFING_CHEESE = {
    price: 10,
    calories: 20
};
Hamburger.STUFFING_SALAD = {
    price: 20,
    calories: 5
};
Hamburger.STUFFING_POT_ATO = {
    price: 15,
    calories: 10
};

Hamburger.TOPPING_MAYO = {
    price: 20,
    calories: 5
};
Hamburger.TOPPING_SPICE = {
    name: "TOPPING_SPICE",
    price: 15,
    calories: 0
};

function HamburgerException(msg) {
    this.message = msg;
    this.message = ('Check hamburger composition');
}

 let hamburger = new Hamburger(Hamburger.SIZE_LARGE,Hamburger.STUFFING_CHEESE);
console.log("Calories: %f", hamburger.calculatePrice());
console.log("Price: %f", hamburger.calculatePrice());
console.log("Calories: %f", hamburger.calculateCalories());
hamburger.addTopping(Hamburger.TOPPING_SPICE);
console.log("Price with sauce: %f", hamburger.calculatePrice());
console.log("Is hamburger large: %s", hamburger.size === Hamburger.SIZE_LARGE);

console.log(hamburger);